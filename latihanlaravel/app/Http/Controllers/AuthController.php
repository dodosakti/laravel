<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function welcome(REQUEST $request){
        //dd($request);
        $first_name = $request->first_name;
        $last_name = $request->last_name;

        return view('welcome', compact('first_name', 'last_name'));
    }
    
    //public function welcome_post(REQUEST){
     //   return view('welcome');
    //}
}
