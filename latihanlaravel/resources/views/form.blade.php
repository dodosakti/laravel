<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label><b>First Name:</b></label> <br><br>
        <input type="text" name="first_name"><br><br>
        <label><b>Last Name:</b></label> <br><br>
        <input type="text" name="last_name">
        <p><b>Gender:</b></p>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br>
        <p><b>Nationality:</b></p>
        <select name="kebangsaan">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Jepang">Jepang</option>
            <option value="Korea Selatan">Korea Selatan</option>
            <option value="Jerman">Jerman</option>
        </select> <br>
        <p><b>Language Spoken:</b></p>
        <input type="checkbox" name="cekbox">Bahasa Indonesia <br>
        <input type="checkbox" name="cekbox">English <br>
        <input type="checkbox" name="cekbox">Other <br>
        <p><b>Bio:</b></p>
        <textarea name="biografi" cols="30" rows="10"></textarea> <br><br>
        <input type="submit">

    </form>
</body>
</html>